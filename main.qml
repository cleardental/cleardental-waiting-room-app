import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtMultimedia 5.12
import QtGraphicalEffects 1.12

ApplicationWindow  {
    width: 1900
    height: 1200
    visible: true
    title: qsTr("Welcome!")
    visibility: ApplicationWindow.FullScreen


    Video {
        source: ""
        anchors.fill: parent
        Component.onCompleted: play();
        fillMode:  VideoOutput.PreserveAspectCrop
    }

    Image {
        id: logo
        source: ""
        width: 512
        height: 512
        fillMode: Image.PreserveAspectFit

    }

    Glow {
        anchors.fill: logo
        radius: 4
        samples: 17
        color: "white"
        source: logo

        SequentialAnimation on radius {
            id: glowSize
            loops: Animation.Infinite
            PropertyAnimation {
                from: 4
                to: 16
                duration: 10000
            }
            PropertyAnimation {
                from: 16
                to: 4
                duration: 10000
            }
        }

        Component.onCompleted: glowSize.running = true
    }

    Label {
        id: currTimeLabel

        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 25
        font.pixelSize: 100
        horizontalAlignment: Qt.AlignRight
        styleColor: "black"
        style: Text.Outline

        function getDateTimeString() {
            var currentTime = new Date();
            text= currentTime.toLocaleTimeString('en-US') + "\n" + currentTime.toLocaleDateString();
        }

        SequentialAnimation {
            id: updateTimeAni
            loops: Animation.Infinite
            ScriptAction {
                script: currTimeLabel.getDateTimeString()
            }
            PauseAnimation { duration: 15 * 1000 }
        }

        Component.onCompleted: updateTimeAni.running = true
    }




}
